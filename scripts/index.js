/*
---- теорія

1) екранування - це виведення на екран спецсимволів в js завдяки \ й використовується воно, відповідно, 
 тоді, коли потрібно відобразити (вивести на екран) спецсимволи

2) function declaration (створюється завдяки ключовому слову "function"),
function expression (тут функція - значення змінної), arrow function (стрілкова функція, використовує у собі "=>")

3) hoisting - явище, за якого оголошення функцій чи змінних переміщується угору, до початку місця їх виконання 
   При цьому функції, які були викликані перед їх оголошенням, будуть працювати (бо функцію
    можна викликати у будь-якому місці), а у змінних переміщується вгору лише їх оголошення й їх також можна
    використовувати до надання значення, але в такому випадку по факту воно буде дорівнювати 'undefined' 

*/

let createNewUser = () => {

    let firstName = prompt("What's your first name?");
    let lastName = prompt("What's your last name?");
    let birthday = prompt("When is your birthday?")

    let newUser = {
        firstName,
        lastName,
        birthday,

        getLogin() {
            return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();    
        },

        getAge() {
            let day = +this.birthday.slice(0, 2); //робимо маніпуляції, бо код прийшов у форматі dd.mm.yyyy
            let month = +this.birthday.slice(3, 5) - 1;
            let year = +this.birthday.slice(6);

            let userBday = new Date(year, month, day); //міняємо місцями 
            let today = new Date();

            let age = today.getFullYear() - userBday.getFullYear(); //дізнаємось скільки в цьому році буде років
            let monthDiff = today.getMonth() - userBday.getMonth();
            if (monthDiff < 0 || (monthDiff === 0 && today.getDate() < userBday.getDate())) { //якщо юзер не відсвяткував др, то йому менше на рік 
                age--;
            }

            return age
        },

        getPassword() {
            return this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6, 10);
        }
    };

    return newUser;
}

let user = createNewUser();
console.log(user.getLogin())
console.log(user.getAge())
console.log(user.getPassword())